\documentclass[../thesis.tex]{subfiles}
\begin{document}
\chapter{Perun} \label{perun}

\emph{\textbf{Per}formance \textbf{Un}der Control} (\emph{Perun}) is an open source \emph{Performance Version System} used for continuous tracking of a project's performance~\cite{fiedorPerunDocumentation}. The project integrates with VCS\footnote{Version Control System} like \emph{Git} or \emph{SVN} and thus keeping performance testing closely tied to it. \emph{Perun} also provides a tool suite for test runs specification, performance metrics collectors, performance data postprocessors and analyzers. One of the goals of this thesis is to integrate the created profiler into Perun as a collector and add appropriate analysers and visualizers for it. To provide a better understanding of the planned out work, an overview of Perun (Section~\ref{perun-overview}), its architecture (Section~\ref{perun-architecture}) and workflow (Section~\ref{perun-workflow}) will be described in the following sections.

\section{Overview} \label{perun-overview}

VCS track how the code base of a project evolves, how the functionality changes, provides versions snapshots (e.g., tags) and often provide additional generic functionality in order to satisfy as many needs as possible. These systems are often flexible enough for keeping track of additional data but they are not optimized for such use-cases. Perun fills the gap for tracking a project's performance by using VCSs to gain insight into a project development history and store its results into a \verb|.perun| directory in the project tree.

Perun advertises the following advantages over the sole use of VCS or databases\cite{fiedorPerunDocumentation}:

\begin{itemize}
    \item \textbf{Context} Results of test runs (performance profiles) are tied to an exact version taken from the VCS, thus providing the necessary context to focus the analysis efforts on the difference in the source code.
    
    \item \textbf{Automation} Manual analysis is a common activity, but not in the context of automated test system. Perun provides a concept of \emph{jobs} for defining test runs and a concept of \emph{hooks} reacting to VCS events (e.g., commit, push, tag).
    
    \item \textbf{Genericity} Every project is different and the requirements put on Perun may vary. Perun thus provides a framework allowing for a straightforward extension of its capabilities (collectors, postprocessors and visualisations) via modules. Its data format (based on a \emph{JSON} notation) is approachable enough for a quick adoption.
    
    \item \textbf{Easy to use} Perun is available as a CLI\footnote{Command Line Interface} application with commands similar to common VCS applications, which lowers the learning curve of using the tool.
\end{itemize}

\section{Architecture} \label{perun-architecture}

Perun's architecture, illustrated in Figure \ref{fig:perun-architecture}, is made of four main components: \emph{logic}, \emph{data}, \emph{check}, and \emph{view}. The main components are complemented by a collection of utility helpers: \emph{vcs}, \emph{workloads}, \emph{fuzzing}, etc.

\begin{figure}[h]
    \label{fig:perun-architecture}
    \centering
    \includegraphics[width=0.75\textwidth]{perun-architecture}
    \caption{An overview of Perun's architecture showing its main components, \emph{VCS} integrations and the flow of data in the program.~\cite{fiedorPerunLightweightPerformance2022}}
\end{figure}

\paragraph{Logic} is responsible for the generation of performance profiles, automation (hooks) and higher-logic manipulations~\cite{FITMT19092, fiedorPerunLightweightPerformance2022}. Its two major components are:

\begin{itemize}
    \item \textbf{Collectors} collecting performance data. They can be implemented either as wrappers of existing profilers and utilities or they can be fully-featured profileres on their own. Notable collectors are: \emph{trace collector}, \emph{complexity collector}, or the simple \emph{time collector}.
    
    \item \textbf{Postprocess} applies various statistical analysers to the collected performance profiles. Notable postproceessors are: \emph{moving average postprocessor}, \emph{regression analysis postprocessor} or \emph{clusterizer postprocessor}
\end{itemize}

\paragraph{Data} is the core of Perun \-- performance profile manipulation is done in this component together with its storage in a VCS. It is the middleware between \emph{logic} and \emph{view}.~\cite{FITMT19092, fiedorPerunLightweightPerformance2022}

\paragraph{Check} implements performance regression detection across different revisions (profiles) of a project. Several detection methods are available due to the high sensitivity of the used statistical techniques to the type of analysed data. Notable detection methods are: \emph{linear regression}, \emph{integral comparison}, or \emph{average amount threshold}.~\cite{FITMT19092}

\paragraph{View} provides means for the collected and processed performance profiles to be visualized. Some of the visualizers are: \emph{bars plot}, \emph{flame graph} or \emph{scatter plot}~\cite{FITMT19092, fiedorPerunLightweightPerformance2022}

\section{Workflow} \label{perun-workflow}

Perun's workflow can be captured in a series of steps the user should follow. The description of these steps is based on~\cite[p.24]{FITMT19092}. See Figure~\ref{fig:perun-flow} for a visualization of Perun's workflow.

The steps of Perun's workflow can be described as follows: 

\begin{enumerate}
    \item Initialize a new Perun repository in a project managed by a VCS using \verb|perun init| (the initialized Perun repository is not managed by the VCS).

    \item Configure the initialized Perun repository and specify the desired collectors, post-processors, workloads and other options.

    \item Make changes to the tracked project and commit them in VCS (may trigger next step automatically depending on the configuration from the previous step).

    \item Collect raw data using either the pre-configured collectors or using a manually selected collector using \verb|perun collect|. The raw data are stored as a profile in the Perun repository.

    \item Optionally process the raw data using either the pre-configured post-processors (e.g., regression analysis, clusterizer) or using a manually selected post-processor using \verb|perun postprocessby|. The processed data are stored in the current profile in the Perun repository.

    \item Compare the current profile with profiles paired with previous revisions of the project using either the pre-configured degradation checker (e.g., integral comparison) or a manually selected degradation checker using \verb|perun check|. The check is done only for matching pairs of tested $binaries + workloads + configurations$. The results of the check are stored as a new object in the Perun repository.

    \item Assess the severity, location and confidence parameters of reported potential performance changes.
\end{enumerate}

\begin{figure}[h]
    \label{fig:perun-flow}
    \centering
    \includegraphics[width=0.75\textwidth]{perun-flow}
    \caption{An illustration of the Perun workflow on a project repository using the Git VCS.~\cite{fiedorPerunLightweightPerformance2022}}
\end{figure}

\end{document}