\documentclass[../thesis.tex]{subfiles}
\begin{document}

\chapter{Background and Related Work} \label{background-related-work}

The task of software profiling is a complex one and researching a novel profiling approach is outside the scope of the thesis. Instead this thesis heavily relies on research conducted in the past, mainly in the area of energy consumption profiling. Past research spans widely from exact approaches to profiling, to analysis of knowledge of developers on the topic of energy profiling. The related work is organized into 3 categories consisting of knowledge of developers about energy consumption (Section \ref{background-related-work-knowledge-of-developers}), programming language specific insights into their energy profile (Section \ref{background-related-work-energy-efficiency}), existing techniques of and approaches to energy profiling (Section \ref{background-related-work-techniques-and-approaches}). Additionally, research of side-channel attacks (Section \ref{background-related-work-side-channel-attacks}) is mentioned as this group of attacks exploits weaknesses in software that could potentially be detected using the results of this thesis.

A great introduction to the scientific research of energy profiling is an article by Pinto and Castor about energy efficiency~\cite{pintoEnergyEfficiencyNew2017} where the authors discuss the general need to focus on the energy efficiency of software, the need to educate developers about the topic and the existence of various research in the area.

\section{Knowledge of developers} \label{background-related-work-knowledge-of-developers}

% knowledge of developers, existence of (or lack of) convenient tools

Profiling is a type of dynamic software analysis that requires from deep knowledge of the problem domain by the developers to discern what information needs to be collected and what the results of the analysis mean. Energy profiling being a specialized domain of profiling makes it potentially an even more difficult type of analysis.

Pinto et al.~\cite{pintoEnergyEfficiencyNew2017} conducted a survey on a sample of software developers with the goal to understanding their perceptions about software energy consumption issues. The results of this survey uncovered that while 67\% of the respondents do care about energy-related features, only in 50\% of the cases when the respondents addressed energy-related issues in a mobile application only a fraction of the respondents used specialized tools and the rest of them depended on a perception of an improvement. The sources of insight into solving the energy-related issues were mainly unempirical, e.g. official documentations, StackOverflow, YouTube, blogs and other sources. These results make it clear that there is a lack of tools for developers to make informed software changes to address energy-related issues.

Pang et al.~\cite{pangWhatProgrammersKnow2016} conducted a survey yielding similar results. The authors summarized the results of the survey into 4 takeways for programmers:

\begin{enumerate*}
  \item having limited awareness of software energy consumption
  \item lacking knowledge of reducing software energy consumption
  \item lacking knowledge of software energy consumption
  \item not being aware of software energy consumption's causes
\end{enumerate*}

The conducted surveys suggest that software developers are under-equipped in both tooling and knowledge to correctly and accurately detect, analyze and fix energy consumption-related issues in software. There is a need for an easy-to-use tool providing detailed insight into the energy profile of software. 

\section{Energy efficiency of software constructs} \label{background-related-work-energy-efficiency}

% efficiency of data structures, thread management

Data structures are a vital part of any software and their knowledge is one of the fundamentals computer science students are taught. Common knowledge is, for example, that time complexity (be it asynmptotic or amortized) of operations differ largely on the type of underlying data container. Time complexity is one of many useful information for determining the performance and potential energy consumption, but it is not the only one. Performance and energy costs of different data structures, abstractions, threading models and more have been the target of a large number of researchers over the years.

% -- different data structures -- significant differences in energy costs, energy savings can be done without having to sacrifice performance, some energy behaviours may not be apparent

Many papers analyze energy profiles of different software constructs. Pinto et al.~\cite{pintoComprehensiveStudyEnergy2016} analyzed the energy efficiency of several implementations of data structures (lists, sets, and maps) in Java and their findings show significant differences. For example, an alternative implementation of a hashtable (\verb|ConcurrentHashMapV8|) yields up to 2.19x energy savings in micro-benchmarks and up to 17\% of savings in real-world benchmarks over the old implementation (\verb|ConcurrentHashMap|). Hasan et al.~\cite{hasanEnergyProfilesJava2016} similarly studied the efficiency of data structures in Java in three different implementations. Using the gained knowledge, the authors created worst-case and best-case scenarios in several open-source libraries and applications. The results ranged from minimal differences of less than 1\%, and up to 300\%, in energy consunmption efficiency. Lima et al.~\cite{limaHaskellGreenLand2016} studied the energy efficiency of different data structures in Haskell. The results showed both marginal (below 1\%) and significant (over 25\%) differences in energy consumption and execution time.  

% -- different representation of data -- evidence for random access being more costly than linear access, reference vs. type vs. value, object-centric vs. attribute-centric (convenience of OO design comes at a cost)

Liu et al.~\cite{liuDataOrientedCharacterizationApplicationLevel2015} conducted an empirical study on how data access patterns, data precision choices, and data organization affect energy consumption in Java. Additionally, the authors studied how various application-level data management features respond to Dynamic Voltage and Frequency Scaling (\emph{DVFS}). Among their findings is that \emph{object-centric} data grouping can be less energy efficient than \emph{attribute-centric} data grouping, and that down-scaling a CPU often leads to worse results in both performance and energy consumption. The down-scaling of a CPU mainly in cases of programs  performing excessive number of I/O operations lead to better results in energy consumption.

% -- different threading model -> different power profile -- the energy costs diffs are not huge but exist

Lima et al.~\cite{limaHaskellGreenLand2016} also studied three different thread management constructs and data sharing primitives in Haskell. The results show differences in energy consumption in tenths of a percent and show that there is no universaly better solution. Choosing the correct construct depends on the context of the specific application and profiling is needed to identify the most suitable constructs. Pinto et al.~\cite{pintoUnderstandingEnergyBehaviors2014} performed an empirical study analyzing energy consumption of different thread management construct. Their findings corroborate to the fact that faster execution times do not always lead to lower energy consumption. In fact, the opposite is usually the case. The curves for the execution time and the consumed energy depending on the number of used cores are not the same. The execution time curve usually display an inverse logarithmic shape while the consumed energy curve display usually a $\Lambda$ shape.

Implementation details of software can significantly affect ways energy consumption of software and there are often no universally applicable solutions yielding optimal results.

\section{Energy profiling techniques and approaches} \label{background-related-work-techniques-and-approaches}

% syscalls, not 100% but good enough for rule-of-thumb rules (4 metrics) and catching changes in the energy profile, only uses strace -> high overhead, student's t-test, linear regression, logistic regression

Aggarwal et al.~\cite{10.5555/2735522.2735546,aggarwalGreenAdvisorToolAnalyzing2015} explored in their work the use of \emph{system calls} to detect changes in the energy profile of software across different versions. Their findings show that system calls are related to power consumption. Combining system call profiles with statistical methods gives a developer an easy-to-use tool capable of accurate assessment. The model authored by Aggarwal et al. achieved at least 80\% accuracy of detecting differences in power consumption across different versions.

% complex solution, accounting policies, SDK/NDK/syscall, power modeling, interesting results, I/O Bundles
Pathak et al.~\cite{pathakWhereEnergySpent2012} created a fine-grained energy profiler for mobile devices. They explored the granularity of energy accounting for the different components in a mobile device and asynchronous power behaviour of some modules (e.g., GPS, WiFi and Bluetooth modules). They based the model, similarly to Aggarwal et al.~\cite{10.5555/2735522.2735546,aggarwalGreenAdvisorToolAnalyzing2015}, on a system call power model which provided them with fine-grained source data. The maximum measurement error reached only 6\% for all tested applications. The sample of tested applications consisted of both simple (e.g. a Sudoku game) and complex (e.g., Angry Birds or Facebook) applications.

% external measurements, post-processing (threads, tail state - assumptions are made, regression analysis), incurred overhead, accuracy, from collection to source-level information
Li et al.~\cite{liCalculatingSourceLine2013} created a solution for calculating the energy consumption of single lines of code in Android applications. Using the combination of external power meters readings, program analysis, and statistical modelling the authors created a highly accurate solution. The error in the calculated energy values were within 10\% of the ground truth measurements and the statistical models had a high $R^2$\footnote{coefficient of determination} average of $0.93$.

The research and energy profiling techniques in this section are further discussed in Chapter \ref{existing-methods-technology}.

\section{Side-channel attacks} \label{background-related-work-side-channel-attacks}

% basic idea, what is spa, what is dpa, latest vulnerability (the use of RAPL to visualize); point out the potential for use to compare energy profiles

Side-channel attacks are a family of attacks relying on physical parameters of the compromised system, such as electromagnetic emissions, execution time, power consumption~\cite{SidechannelAttacksExplained} and others. Two of the most common techniques of side-channel attacks are the following.

\paragraph{Simple Power Analysis (\emph{SPA})} is a technique involving direct interpretation of power consumption measurements collected during critical operations. The collected measurements are interpreted directly and allow to differentiate between different kinds of operations (e.g., multiplication vs. squaring). The technique is simple as it does not involve any post-processing or more involved measuring. Defense mechanisms in hardware (e.g., protective cases) and software (e.g., branch-less programming, noise generation) are quite effective at limiting the susceptibility to the attack. The intention is to lower the power consumption variations such that \emph{SPA} will not yield any key material.~\cite{kocherIntroductionDifferentialPower1998, kocherDifferentialPowerAnalysis1999}

\paragraph{Differential Power Analysis (\emph{DPA})} is a technique consisting of two phases: data collection and data analysis. The data collection phase is similar to \emph{SPA} and the collected information is usually the device's energy consumption. The second phase involves statistical analysis and error correction techniques. Their application enables to identify key material at a much smaller scale. Defence against \emph{DPA} involves several approaches, e.g., reducing signal sizes, introducing noise into power consumption measurements, and designing cryptosystems with realistic assumptions about the underlying hardware.~\cite{kocherIntroductionDifferentialPower1998, kocherDifferentialPowerAnalysis1999} 

A recent example of a real side-channel attack is \emph{hertzbleed}~\cite{wangHertzbleedTurningPower2022} which is based on a proof that \emph{Dynamic Voltage and Frequency Scaling} (\emph{DVFS}), power consumption and currently processed data directly affect each othe. This allows for a remote timing attack where the attack side-channel is the execution time.

% To make Hertzbleed relevant this work needs to be connected to DVFS somehow. Need to read more about it.

Prevention of side-channel attacks is difficult and involves several, often only partially related, approaches. One of them is lowering power consumption variations. The result of this thesis could potentially help to uncover areas for improvement in this regard. Integration in Perun could provide high accuracy (using post-processing and visualization modules) and automation (by integrating Perun-based testing into \emph{CI} systems).

\end{document}