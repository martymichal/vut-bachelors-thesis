\documentclass[../thesis.tex]{subfiles}
\begin{document}

\chapter{eBPF} \label{ebpf}

\epigraph{Super powers have finally come to Linux.}{Brendann Gregg}

\emph{eBPF} is a technology allowing to run sandboxed programs in a privileged context like the Linux kernel~\cite{WhatEBPFIntroduction}. It allows to extend the capabilities of the kernel without changing the kernel source code or loading custom kernel modules. Efficiency and security of the executed programs are ensured thanks to JIT\footnote{Just-In-Time} compilation and a verification engine. 

\section{Overview}

TODO: BPF bytecode, verifier, maps, kernel-space/user-space interaction, capabilities

\begin{figure}[h]
    \includegraphics[width=\textwidth]{ebpf-architecture}
    \caption{eBPF architecture}
\end{figure}

\section{History of eBPF}

\emph{eBPF} historically standed for \emph{extended BPF} but that has not been the case for a while now. While originating in the \emph{Berkley Packet Filters} (\emph{BPF}) it has evolved into a technology of its own.

Originally, \emph{BPF} was designed as a very simple virtual machine intended for filtering packets in kernel-space. The provided packet filters were compiled into programs which were executed in a register-based virtual machine running inside of a kernel. This has allowed to transition from the until then common approach of implementing packet filters in user-space which required copying of data across the kernel/user-space protection boundary. The created technology yielded up to 20 times faster filter evaluation and up to 100 times better performance than Sun’s \emph{NIT}\footnote{Network Interface Tap}.~\cite{mccannelBSDPacketFilter1993}

A virtual machine with registers and close integration with the kernel provided a good foundation for further extensions of the module in the Linux kernel. Specifically, the virtual machine architecture was updated to match more closely modern architectures, the number of registers increased and the number of kernel subsystems interfacing with the virtual machine increased as well. In Linux 3.18, the \verb|bpf()| syscall was introduced allowing for loading and running eBPF programs in the form of a bytecode during system runtime. In Linux 4.4 the \verb|bpf()| syscall was made available to non-root users as well by extending and fortifying the eBPF verifier that checks all loaded eBPF programs~\cite{ThoroughIntroductionEBPF,BPFSystemCall,UnprivilegedBpfLWN}.

\subsection{Adoption of eBPF}

\emph{BPF} was originally~\cite{mccanneBSDPacketFilter1993} designed for UNIX systems and \emph{eBPF} was a result of work done on the Linux kernel. By design, it is also meant to live as close as possible to the kernel but the popularity and practicality of the technology has motivated the creation of projects like \emph{ubpf}~\cite{UBPF2023} which is an implementation of a \emph{eBPF} virtual machine in userspace licensed under the Apache license. Microsoft also introduced an implementation of \emph{eBPF} for Microsoft Windows called \emph{eBPF for Windows} which utilizes \emph{ubpf}~\cite{EBPFWindows2023,gaddehosurMakingEBPFWork2021, ImplementingEBPFWindows}.

\section{Developer toolchains}

Writing \emph{eBPF} bytecode by hand is not a recommended venture since the kernel ecosystem keeps evolving at an incredible pace. A possible solution is to leverage abstractions built on top of \emph{eBPF} like \emph{bcc} (\ref{ebpf-bcc}), \emph{bpftrace} (\ref{ebpf-bpftrace}) and \emph{libbpf} (\ref{ebpf-libbpf}), providing a more expressive environment for writing \emph{eBPF} programs. The abstractions universally depend on the Clang/LLVM toolchain implementing an \emph{eBPF} bytecode backend for compilation of \emph{eBPF} programs.

\subsection{BCC} \label{ebpf-bcc}

\begin{wrapfigure}{r}{0.20\textwidth}
    \includegraphics[width=0.20\textwidth]{ebpf-bcc-logo}
\end{wrapfigure}

\emph{BPF Compiler Collection} (\emph{BCC}) is a toolchain providing C, Python and Lua interface for writing \emph{eBPF} programs. The \emph{BCC} toolchain is based on the LLVM toolchain. It provides a BPF-specific front-end for C, making it easier to write valid \emph{eBPF} programs. Along the C language front-end, additional front-ends for Python and Lua are provided~\cite{BPFCompilerCollection2022}.

Since 2020, the Python interface of \emph{BCC} is considered deprecated for writing new performance tools~\cite{BPFBinariesBTF}. It is recommended to use the C interface and \emph{bpftrace} for quick \emph{eBPF} scripting.

\subsection{bpftrace} \label{ebpf-bpftrace}

\emph{bpftrace} is a high-level frontend for \emph{eBPF} utilizing \emph{BCC} for interfacing with the \emph{eBPF} Linux system. It is ideal for writing \emph{one-liners} and short scripts. The \emph{bpftrace} language is inspired by \emph{awk}, \emph{C} language and tracers like \emph{DTrace} and \emph{SystemTap}~\cite{IovisorBpftrace}.

An example of a \emph{bpftrace} one-liner counting the number of page faults by process is:

\begin{lstlisting}[language=sh]
$ bpftrace -e 'software:faults:1 { @[comm] = count(); }'
\end{lstlisting}

\subsection{libbpf} \label{ebpf-libbpf}

\emph{libbpf} is a user-space \emph{C}/\emph{C++} \emph{eBPF} loader library providing APIs for interacting with the \emph{eBPF} Linux system. \emph{libbpf} bootstraps the loaded \emph{eBPF} object file, allowing for hooking up to different phases of \emph{eBPF} program lifetime, and providing useful API similarly to \emph{BCC} (albeit not as high-level). The development of \emph{libbpf} over the years lead to closing the gap in features between \emph{BCC}, ironing out issues in the API or addressing scenarios of incompatibilities between different versions of the kernel. This work culminated in the release of version 1.0 in August 2022~\cite{JourneyLibbpf}.
    
The lifetime of \emph{eBPF} programs has 4 phases for which \emph{libbpf} provides a way to define them and execute them. These phases are~\cite{nakryikoBCCLibbpfConversion}:

\begin{itemize}
    \item \textbf{Open phase} loads up the BPF object file into memory, discovers BPF programs, maps and global variables. It allows to make adjustments to the discovered structures because the program has not yet been loaded into the kernel.

    \item \textbf{Load phase} creates BPF maps, verifies the programs and loads them into the kernel, however, the programs are not executed just yet. At this phase it is still possible to adjust the state of BPF maps.

    \item \textbf{Attachment phase} starts the BPF program by attaching it to its defined hook points.

    \item \textbf{Teardown phase} detaches and unloads BPF programs from the kernel. All BPF resources (e.g., maps) are freed.
\end{itemize}

\section{BPF CO-RE}

\emph{BPF Compile Once - Run Everywhere} (\emph{BPF CO-RE})~\cite{BPFPortabilityCORE} is an approach to creating \emph{eBPF} programs that solves many issues with portability of \emph{eBPF} programs. The technology was introduced at the LSF conference in 2019~\cite{foobar}. Many \emph{eBPF} tools originally written using BCC or other toolchains have already been converted to \emph{BPF CO-RE}.~\cite{BPFPortabilityCORE}

Portability of \emph{eBPF} programs is troublesome. The user-written \emph{eBPF} programs need to work within the set kernel environment over which they have little to no control. Any program requiring access to raw internal kernel data (e.g., \verb|struct trace_event_raw_sys_exit|) can not rely on any notion of stability from the kernel. \emph{BCC} overcomes this problem by embedding an \emph{eBPF} program in a user-space binary and compiling it on-the-fly when requested. The compilation uses \emph{Clang/LLVM}, embedded in the library, and kernel headers. These requirements lead to unnecessarily large binaries (each user-space \emph{eBPF} program has \emph{Clang/LLVM} embedded) and unstable programs (e.g., unhandled cases of renamed structures across kernel versions). Also, kernel headers are not installed on systems by default which are required for \emph{Clang/LLVM} to be able to compile an \emph{eBPF} program.\cite{BPFPortabilityCORE}

\emph{BPF CO-RE} overcomes the portability problem by leveraging several functionalities in the used components: kernel info format (\emph{BTF}), compiler (\emph{Clang}), and user-space \emph{eBPF} loader library (\emph{libbpf}).

\subsection{BTF}

\emph{BTF} stands for \emph{BPF Type Format}. It is a debugging data format which is an alternative to the \emph{DWARF} format. It achieves up to 100x size reduction over \emph{DWARF} while containing all necessarry type information of C programs. Having such a space-efficient data format makes it feasible for having it available by default. The information is available at path \verb|/sys/kernel/btf/vmlinux| and can be used to generate a C header file using the tool \verb|bpftool|.\cite{BPFPortabilityCORE}

\subsection{Clang}

The \emph{Clang} compiler was extended with a feature. It makes it possible to track field existence, removal, offset relocation or size change. These bits of information are then emited for the \emph{eBPF} program loader to pick up and use it for making the necessarry adjustments.\cite{BPFPortabilityCORE,HOWTOBCCLibbpf}

\subsection{libbpf}

Before loading an \emph{eBPF} program and then submitting it to the verificator, \emph{libbpf} inspects the \emph{eBPF} object file and, if necessarry, makes adjustments to it to match the \emph{BTF} information of the running kernel. These adjustments are done completely transparently and automatically.\cite{BPFPortablityCORE}

\section{eBPF in practice}

Since its creation \emph{eBPF} has been constantly growing in popularity and the number of use cases for it only keep growing. Nowadays it is possible to not just monitor and profile but also change the behaviour of the kernel at runtime\cite{HIDBPFLinuxKernela}. Some noteworthy \emph{eBPF} projects are:

\begin{enumerate}\item
    \item \emph{libbpf-tools} and \emph{bcc-tools} are collections of quite small but very useful \emph{eBPF} programs contributed by the members of the community available for all users to use for profiling.\cite{BPFCompilerCollection2022}
    \item \emph{ebpf_exporter} is a metrics collector/exporter for Prometheus\cite{prometheusPrometheusMonitoringSystem} created by CloudFlare.\cite{EbpfExporter2023}
    \item \emph{bpfilter} is a direct competitor of \emph{iptables} and \emph{nftables}. It offers a compatibility layer translating iptables rules into \emph{eBPF} programs while also offering the possiblity of writing firewall rules in C.\cite{BPFComesFirewalls}
     \cite \emph{wachy} is a dynamic tracing profiler providing a TUI\footnote{Text-based User Interface}.{Wachy2022}
    
\end{enumerate}

\end{document}